package com.yingside.test;

import com.yingside.dao.DeptDao;
import com.yingside.po.Dept;
import com.yingside.po.Employee;
import com.yingside.util.SqlSessionFactoryUtil;
import com.yingside.util.SqlSessionUtil;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;


public class TestDept {
    private Logger logger = Logger.getLogger(TestDept.class);
    private SqlSession sqlSession;

    @Before
    public void init(){
        //读取核心配置文件
        InputStream ins = this.getClass().getClassLoader().getResourceAsStream("mybatis-config.xml");
        //生成SqlSessionFactory对象
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(ins);
        //获取sqlSession对象
        sqlSession = factory.openSession();
    }

    @Test
    public void selectDepts(){
        String stmt = "com.yingside.mapper.DeptMapper.selectDepts";
        List<Dept> deptList = sqlSession.selectList(stmt);

        logger.info(deptList);

    }

    @Test
    public void selectDepts2(){
        String stmt = "com.yingside.mapper.DeptMapper.selectAllDept";
        List<Dept> deptList = sqlSession.selectList(stmt);

        for (int i = 0; i < deptList.size(); i++) {
            logger.info(deptList.get(i).getDeptName());
            logger.info(deptList.get(i).getEmployeeList().get(0).getEmpName());
        }

    }

    @Test
    public void selectDeptEmployee(){
        SqlSession sqlSession = SqlSessionUtil.getSqlSession();
        DeptDao mapper = sqlSession.getMapper(DeptDao.class);
        List<Dept> depts = mapper.selectDeptEmployees();

        logger.info(depts);
    }
}
