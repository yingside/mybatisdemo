package com.yingside.test;

import com.yingside.po.Dept;
import com.yingside.po.Employee;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;


public class TestEmployee {
    private Logger logger = Logger.getLogger(TestEmployee.class);
    private SqlSession sqlSession;

    @Before
    public void init(){
        //读取核心配置文件
        InputStream ins = this.getClass().getClassLoader().getResourceAsStream("mybatis-config.xml");
        //生成SqlSessionFactory对象
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(ins);
        //获取sqlSession对象
        sqlSession = factory.openSession();
    }

    @Test
    public void selectEmployees(){
        String stmt = "com.yingside.mapper.EmployeeMapper.selectEmployees";
        List<Employee> employeeList = sqlSession.selectList(stmt);

        logger.info(employeeList);
    }

    @Test
    public void selectEmployees2(){
        String stmt = "com.yingside.mapper.EmployeeMapper.selectAllEmployees";
        List<Employee> employeeList = sqlSession.selectList(stmt);

        logger.info(employeeList.get(0).getEmpName());

        logger.info(employeeList.get(0).getDept().getDeptName());
    }

    @Test
    public void selectEmployeeById(){
        String stmt = "com.yingside.mapper.EmployeeMapper.selectEmployeeById";
        Employee employee1 = sqlSession.selectOne(stmt,1);
        System.out.println("-------------员工信息-------------");
        logger.info(employee1);

        System.out.println("-------------修改部门信息-------------");
        Dept dept = new Dept();
        dept.setDeptId(2);
        dept.setDeptName("餐厅部");
        stmt = "com.yingside.mapper.DeptMapper.updateDeptByIdSelective";
        int m = sqlSession.update(stmt,dept);
        logger.info(m);
        sqlSession.commit();

        System.out.println("-------------再次查询员工信息-------------");
        stmt = "com.yingside.mapper.EmployeeMapper.selectEmployeeById";
        Employee employee2 = sqlSession.selectOne(stmt,1);
        logger.info(employee2);

    }
}
