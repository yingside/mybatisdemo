package com.yingside.test;

import com.yingside.dao.UserDao;
import com.yingside.po.User;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;

public class TestUserMapper {
    private Logger logger = Logger.getLogger(TestJunitUser.class);
    private SqlSession sqlSession;

    @Before
    public void init(){
        //读取核心配置文件
        InputStream ins = this.getClass().getClassLoader().getResourceAsStream("mybatis-config.xml");
        //生成SqlSessionFactory对象
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(ins);
        //获取sqlSession对象
        sqlSession = factory.openSession();
    }

    @Test
    public void getAll(){
        UserDao userDao = sqlSession.getMapper(UserDao.class);
        List<User> all = userDao.getAll();
        logger.info(all);
    }

    @Test
    public void addUser(){
        UserDao userDao = sqlSession.getMapper(UserDao.class);
        User user = new User();
        user.setUserTel("21321321312");
        user.setUsername("aaaa");
        user.setRegistrationTime("2020-11-11");
        user.setPassword("11111");
        int m = userDao.addUser(user);

        sqlSession.commit();

        logger.info(m);
    }
}
