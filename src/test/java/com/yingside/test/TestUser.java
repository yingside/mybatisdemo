package com.yingside.test;

import com.yingside.po.User;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import java.io.InputStream;
import java.util.List;
public class TestUser {

    public static void main(String[] args) {
        TestUser user = new TestUser();
        user.run();
    }

    public void run(){
        //读取核心配置文件
        InputStream ins = this.getClass().getClassLoader().getResourceAsStream("mybatis-config.xml");
        //生成SqlSessionFactory对象
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(ins);
        //获取sqlSession对象
        SqlSession sqlSession = factory.openSession();

        //找到查询方法的全名
        String stmt = "com.yingside.mapper.UserDao.getAll";
        
        List<User> users = sqlSession.selectList(stmt,User.class);

        for (int i = 0; i < users.size(); i++) {

        }

        sqlSession.close();

    }

    public void testUserById(){
        //读取核心配置文件
        InputStream ins = this.getClass().getClassLoader().getResourceAsStream("mybatis-config.xml");
        //生成SqlSessionFactory对象
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(ins);
        //获取sqlSession对象
        SqlSession sqlSession = factory.openSession();

        String stmt = "com.yingside.mapper.UserDao.selectUserById";

        User user = sqlSession.selectOne(stmt,3);

        System.out.println("user = " + user);

        sqlSession.close();
    }

    public void TestAddUser(){
        //读取核心配置文件
        InputStream ins = this.getClass().getClassLoader().getResourceAsStream("mybatis-config.xml");
        //生成SqlSessionFactory对象
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(ins);
        //获取sqlSession对象
        SqlSession sqlSession = factory.openSession();

        String stmt = "com.yingside.mapper.UserDao.addUser";

        User user = new User();
        user.setUsername("韦小宝");
        user.setPassword("123456");
        user.setUserTel("13888888888");
        user.setRegistrationTime("2020-11-11");

        //新增返回值(几行数据受影响)
        int m = sqlSession.insert(stmt,user);

        sqlSession.commit();

        sqlSession.close();

        System.out.println("m = " + m);

    }
}
