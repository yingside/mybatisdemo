package com.yingside.test;

import com.yingside.po.QueryBean;
import com.yingside.po.User;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class TestJunitUser {
    private Logger logger = Logger.getLogger(TestJunitUser.class);
    private SqlSession sqlSession;

    @Before
    public void init(){
        //读取核心配置文件
        InputStream ins = this.getClass().getClassLoader().getResourceAsStream("mybatis-config.xml");
        //生成SqlSessionFactory对象
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(ins);
        //获取sqlSession对象
        sqlSession = factory.openSession();
    }

    @Test
    public void delete(){
        String stmt = "com.yingside.mapper.UserDao.deleteById";
        //几行受影响
        int m = sqlSession.delete(stmt, 8);
        //增删改必须commit
        sqlSession.commit();
        sqlSession.close();

        logger.info(m);
    }

    @Test
    public void addUser(){
        String stmt = "com.yingside.mapper.UserDao.addUser";

        User user = new User();
        user.setUsername("茅十八");
        user.setPassword("123456");
        user.setUserTel("13888888883");
        user.setRegistrationTime("1998-11-11");

        //新增返回值(几行数据受影响)
        int m = sqlSession.insert(stmt,user);

        sqlSession.commit();

        sqlSession.close();

        logger.info(m);
        logger.info("新增id是:" + user.getId());
    }

    @Test
    public void findByUsername(){
        String username = "%张%";
        String stmt = "com.yingside.mapper.UserDao.selectUserByUsername";
        List<User> users = sqlSession.selectList(stmt, username);

        logger.info(users);
    }

    @Test
    public void findByUserSelective(){

        String stmt = "com.yingside.mapper.UserDao.selectUserBySelective";

        User user = new User();
//        user.setId(2);
        user.setUsername("张");
        user.setUserTel("138888");

        List<User> users = sqlSession.selectList(stmt, user);

        logger.info(users);
    }

    @Test
    public void findByUserSelective2(){

        String stmt = "com.yingside.mapper.UserDao.selectUserBySelective2";

        User user = new User();
        user.setId(2);
        user.setUsername("张");
        user.setUserTel("138888");

        List<User> users = sqlSession.selectList(stmt, user);

        logger.info(users);
    }

    @Test
    public void findByUserSelective3(){

        String stmt = "com.yingside.mapper.UserDao.selectUserBySelective3";

        User user = new User();
        user.setId(2);
        user.setUsername("张");
        user.setUserTel("138888");

        List<User> users = sqlSession.selectList(stmt, user);

        logger.info(users);
    }

    @Test
    public void updateUser(){

        String stmt = "com.yingside.mapper.UserDao.updateUser";

        User user = new User();
        user.setId(13);
        user.setUsername("张十九");
        user.setUserTel("13888876543");

        int m = sqlSession.update(stmt, user);

        sqlSession.commit();
        sqlSession.close();
        logger.info(m);
    }

    @Test
    public void updateUserBySelective(){

        String stmt = "com.yingside.mapper.UserDao.updateUserBySelective2";

        User user = new User();
        user.setId(13);
        user.setUsername("十111");
        user.setUserTel("13888876542");

        int m = sqlSession.update(stmt, user);

        sqlSession.commit();
        sqlSession.close();
        logger.info(m);
    }

    @Test
    public void selectUserByIds(){

        String stmt = "com.yingside.mapper.UserDao.selectUserByIds";

        QueryBean queryBean = new QueryBean();
        List<Integer> ids = new ArrayList<>();
        ids.add(1);
        ids.add(2);
        ids.add(3);
        ids.add(4);
        queryBean.setIds(ids);

        List<User> users = sqlSession.selectList(stmt, queryBean);

        logger.info(users);
    }

    @Test
    public void selectUserByIds2(){

        String stmt = "com.yingside.mapper.UserDao.selectUserByIds2";

        QueryBean queryBean = new QueryBean();
        List<Integer> ids = new ArrayList<>();
        ids.add(1);
        ids.add(2);
        ids.add(3);
        ids.add(4);
        queryBean.setIds(ids);

        List<User> users = sqlSession.selectList(stmt, queryBean);

        logger.info(users);
    }

    @Test
    public void selectUserByRegister(){

        String stmt = "com.yingside.mapper.UserDao.selectUserByRegister";

        QueryBean queryBean = new QueryBean();
       queryBean.setBeginTime("2018-12-17");
       queryBean.setEndTime("2018-12-20");

        List<User> users = sqlSession.selectList(stmt, queryBean);

        logger.info(users);
    }

    @Test
    public void selectUserById(){
        String stmt = "com.yingside.mapper.UserDao.selectUserById";
        System.out.println("----------------第一次查询-------------------");


        User user1 = sqlSession.selectOne(stmt, 1);
        logger.info(user1);
        //commit的时候会自动清空缓存
//        sqlSession.commit();
//        sqlSession.clearCache();
        System.out.println("----------------第一次查询-------------------");

        System.out.println("----------------第二次查询-------------------");


        User user2 = sqlSession.selectOne(stmt, 1);
        logger.info(user2);
        sqlSession.commit();
        System.out.println("----------------第二次查询-------------------");
    }
}
