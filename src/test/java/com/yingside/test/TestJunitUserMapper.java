package com.yingside.test;

import com.yingside.dao.UserMapper;
import com.yingside.po.QueryBean;
import com.yingside.po.User;
import com.yingside.util.SqlSessionUtil;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class TestJunitUserMapper {
    private Logger logger = Logger.getLogger(TestJunitUserMapper.class);
    private SqlSession sqlSession;

    @Before
    public void init(){
        //读取核心配置文件
        InputStream ins = this.getClass().getClassLoader().getResourceAsStream("mybatis-config.xml");
        //生成SqlSessionFactory对象
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(ins);
        //获取sqlSession对象
        sqlSession = factory.openSession();
    }

    @Test
    public void getAll(){
        SqlSession sqlSession = SqlSessionUtil.getSqlSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        List<User> all = mapper.getAll();
        logger.info(all);
    }

    @Test
    public void findUesr(){
        SqlSession sqlSession = SqlSessionUtil.getSqlSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        List<User> all = mapper.findUser("138888","黄");
        logger.info(all);
    }

    @Test
    public void deleteById(){
        SqlSession sqlSession = SqlSessionUtil.getSqlSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        int m = mapper.delete(14);
        SqlSessionUtil.commit();
        logger.info(m);
    }

    @Test
    public void addUser(){
        SqlSession sqlSession = SqlSessionUtil.getSqlSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);

        User user = new User();
        user.setUserTel("123213213");
        user.setUsername("aaaa");
        user.setPassword("1111");
        user.setRegistrationTime("2020-11-11");

        int add = mapper.add(user);

        SqlSessionUtil.commit();
        logger.info(user.getId());
    }


    @Test
    public void updateUser(){
        SqlSession sqlSession = SqlSessionUtil.getSqlSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);

        User user = new User();
        user.setUserTel("2222");
        user.setUsername("bbbb");
//        user.setPassword("3333");
//        user.setRegistrationTime("2020-01-01");
        user.setId(15);

        int m = mapper.updateSelective(user);

        SqlSessionUtil.commit();
        logger.info(m);
    }

    @Test
    public void updateUserProvider(){
        SqlSession sqlSession = SqlSessionUtil.getSqlSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);

        User user = new User();
        user.setUserTel("333");
        user.setUsername("cccc");
//        user.setPassword("3333");
//        user.setRegistrationTime("2020-01-01");
        user.setId(15);

        int m = mapper.updateSelectiveProvider(user);

        SqlSessionUtil.commit();
        logger.info(m);
    }

    @Test
    public void addUserProvider(){
        SqlSession sqlSession = SqlSessionUtil.getSqlSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);

        User user = new User();
        user.setUserTel("123213213");
        user.setUsername("gggg");
        user.setPassword("66666");
        user.setRegistrationTime("2020-11-11");

        int add = mapper.addProvider(user);

        SqlSessionUtil.commit();
        logger.info(user.getId());
    }
}
