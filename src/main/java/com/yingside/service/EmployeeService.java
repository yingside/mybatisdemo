package com.yingside.service;

import com.yingside.po.Employee;
import com.yingside.util.Result;
import com.yingside.vo.EmployeeVO;

import java.util.List;

public interface EmployeeService {
    Result<List<EmployeeVO>> getEmployeeByPage(int pageNo, int pageSize);
}
