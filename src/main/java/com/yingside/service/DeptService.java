package com.yingside.service;

import com.yingside.util.Result;
import com.yingside.vo.DeptVO;

import java.util.List;

public interface DeptService {

    Result<List<DeptVO>> getAllDeptEmployee();
}
