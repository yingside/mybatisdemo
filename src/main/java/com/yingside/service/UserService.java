package com.yingside.service;

import com.yingside.po.User;
import com.yingside.util.Result;

import java.util.List;

public interface UserService {
    List<User> getAll();
    Result<List<User>> getUserByPage(int pageNo, int pageSize);
}
