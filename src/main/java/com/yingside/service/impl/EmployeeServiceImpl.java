package com.yingside.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yingside.dao.EmployeeDao;
import com.yingside.po.Employee;
import com.yingside.service.EmployeeService;
import com.yingside.util.Result;
import com.yingside.util.SqlSessionUtil;
import com.yingside.vo.EmployeeVO;
import org.apache.ibatis.session.SqlSession;

import java.util.ArrayList;
import java.util.List;

public class EmployeeServiceImpl implements EmployeeService {
    @Override
    public Result<List<EmployeeVO>> getEmployeeByPage(int pageNo, int pageSize) {
        SqlSession sqlSession = SqlSessionUtil.getSqlSession();
        EmployeeDao employeeDao = sqlSession.getMapper(EmployeeDao.class);
        Result<List<EmployeeVO>> result = new Result<>();

        //分页拦截器
        //拦截器会在dao层执行完之后再执行
        PageHelper.startPage(pageNo,pageSize);
        //本身employees在dao层获取的是全部数据
        //但是经过分页拦截之后实际只是存储了3条数据
        //但是分页拦截对象其实已经保存了List<Employee>对象的全部数据
        List<Employee> employees = employeeDao.selectEmployees();

        if(employees.size() <= 0){
            result.setCode(-1);
            result.setMessage("failure");
            result.setData(null);
            result.setCount(0L);
        }
        else{
            List<EmployeeVO> employeeVOS = new ArrayList<>();
            for (int i = 0; i < employees.size(); i++) {
                Employee employee = employees.get(i);
                EmployeeVO employeeVO = new EmployeeVO();
                employeeVO.setEmpId(employee.getEmpId());
                employeeVO.setEmpName(employee.getEmpName());
                employeeVO.setEmpBirthday(employee.getEmpBirthday());
                employeeVO.setEmpEducation(employee.getEmpEducation());
                employeeVO.setEmpTel(employee.getEmpTel());

                employeeVO.setDeptId(employee.getDept().getDeptId());
                employeeVO.setDeptName(employee.getDept().getDeptName());
                employeeVOS.add(employeeVO);
            }

            PageInfo<Employee> pageInfo = new PageInfo<>(employees);


            result.setCode(0);
            result.setMessage("success");
            result.setData(employeeVOS);
            result.setCount(pageInfo.getTotal());
        }

        return result;
    }
}
