package com.yingside.service.impl;

import com.yingside.dao.DeptDao;
import com.yingside.po.Dept;
import com.yingside.po.Employee;
import com.yingside.service.DeptService;
import com.yingside.util.Result;
import com.yingside.util.SqlSessionUtil;
import com.yingside.vo.DeptVO;
import org.apache.ibatis.session.SqlSession;

import java.util.ArrayList;
import java.util.List;

public class DeptServiceImpl implements DeptService {
    private DeptDao deptDao;
    @Override
    public Result<List<DeptVO>> getAllDeptEmployee() {
        SqlSession sqlSession = SqlSessionUtil.getSqlSession();
        DeptDao mapper = sqlSession.getMapper(DeptDao.class);
        List<Dept> depts = mapper.selectDeptEmployees();

        List<DeptVO> deptVOS = new ArrayList<>();

        for (int i = 0; i < depts.size(); i++) {
            Dept dept = depts.get(i);
            for (int j = 0; j < dept.getEmployeeList().size(); j++) {
                DeptVO vo = new DeptVO();
                vo.setDeptId(dept.getDeptId());
                vo.setDeptName(dept.getDeptName());
                vo.setDeptInfo(dept.getDeptInfo());
                vo.setDeptCreateDate(dept.getDeptCreateDate());
                Employee emp = dept.getEmployeeList().get(j);
                vo.setEmpId(emp.getEmpId());
                vo.setEmpName(emp.getEmpName());
                deptVOS.add(vo);
            }
        }

        Result<List<DeptVO>> result = new Result<>();
        result.setCode(0);
        result.setMessage("success");
        result.setCount(Long.parseLong(deptVOS.size() + ""));
        result.setData(deptVOS);

        return result;
    }
}
