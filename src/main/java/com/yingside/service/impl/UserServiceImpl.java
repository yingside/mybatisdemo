package com.yingside.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yingside.dao.UserDao;
import com.yingside.po.User;
import com.yingside.service.UserService;
import com.yingside.util.Result;
import com.yingside.util.SqlSessionUtil;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

public class UserServiceImpl implements UserService {
    private UserDao userDao;
    @Override
    public List<User> getAll() {
        SqlSession sqlSession = SqlSessionUtil.getSqlSession();
        userDao = sqlSession.getMapper(UserDao.class);

        List<User> users = userDao.getAll();

        return users;
    }

    @Override
    public Result<List<User>> getUserByPage(int pageNo, int pageSize) {
        SqlSession sqlSession = SqlSessionUtil.getSqlSession();
        userDao = sqlSession.getMapper(UserDao.class);
        //注意：这句话一定要放在读取数据的前面，才能实现拦截
        PageHelper.startPage(pageNo,pageSize);

        List<User> users = userDao.getAll();

        PageInfo<User> pageInfo = new PageInfo<User>(users);
        Result<List<User>> result = new Result<>();
        result.setCode(0);
        result.setMessage("success");
        result.setCount(pageInfo.getTotal());
        result.setData(pageInfo.getList());
        return result;
    }
}
