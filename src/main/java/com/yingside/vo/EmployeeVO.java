package com.yingside.vo;

import lombok.Data;

import java.util.Date;

@Data
public class EmployeeVO {
    private int empId;
    private String empName;
    private String empTel;
    private String empEducation;
    private Date empBirthday;
    private Integer deptId;
    private String deptName;
}
