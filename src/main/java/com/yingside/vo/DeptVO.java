package com.yingside.vo;

import lombok.Data;

import java.util.Date;

@Data
public class DeptVO {
    private Integer deptId;
    private String deptName;
    private String deptInfo;
    private Date deptCreateDate;
    private Integer empId;
    private String empName;
}
