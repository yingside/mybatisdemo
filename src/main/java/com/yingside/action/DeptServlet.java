package com.yingside.action;

import com.google.gson.Gson;
import com.yingside.service.DeptService;
import com.yingside.service.impl.DeptServiceImpl;
import com.yingside.util.Result;
import com.yingside.vo.DeptVO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "DeptServlet",urlPatterns = "/dept.do")
public class DeptServlet extends HttpServlet {
    private DeptService deptService = new DeptServiceImpl();
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
        Result<List<DeptVO>> allDeptEmployee = deptService.getAllDeptEmployee();
        Gson gson = new Gson();
        String s = gson.toJson(allDeptEmployee);
        response.getWriter().print(s);
    }
}
