package com.yingside.action;

import com.google.gson.Gson;
import com.yingside.po.User;
import com.yingside.service.UserService;
import com.yingside.service.impl.UserServiceImpl;
import com.yingside.util.Result;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "UserServlet",urlPatterns = "/user.do")
public class UserServlet extends HttpServlet {
    private UserService userService = new UserServiceImpl();
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String method = request.getParameter("method");
        if(method == null || "".equalsIgnoreCase(method)){
            getAll(request,response);
        }
        else if(method.equalsIgnoreCase("pageInfo")){
            pageInfo(request,response);
        }
    }

    private void pageInfo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        response.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
        String sPageNo = request.getParameter("pageNo");
        String sPageSize = request.getParameter("pageSize");

        int pageNo = Integer.parseInt(sPageNo);
        int pageSize = Integer.parseInt(sPageSize);

        Result<List<User>> result = userService.getUserByPage(pageNo, pageSize);
        Gson gson = new Gson();
        String s = gson.toJson(result);
        response.getWriter().print(s);
    }

    private void getAll(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        response.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
        List<User> users = userService.getAll();
        Gson gson = new Gson();
        String s = gson.toJson(users);
        response.getWriter().print(s);
    }
}
