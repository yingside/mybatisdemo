package com.yingside.action;

import com.google.gson.Gson;
import com.yingside.service.EmployeeService;
import com.yingside.service.impl.EmployeeServiceImpl;
import com.yingside.util.Result;
import com.yingside.vo.EmployeeVO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "EmployeeServlet",urlPatterns = "/employee.do")
public class EmployeeServlet extends HttpServlet {
    private EmployeeService employeeService = new EmployeeServiceImpl();
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String method = request.getParameter("method");
        if(method == null || "".equalsIgnoreCase(method)){
            getEmployeeByPage(request,response);
        }
    }

    private void getEmployeeByPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        response.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
        String sPageNo = request.getParameter("pageNo");
        String sPageSize = request.getParameter("pageSize");

        int pageNo = Integer.parseInt(sPageNo);
        int pageSize = Integer.parseInt(sPageSize);
        Result<List<EmployeeVO>> result = employeeService.getEmployeeByPage(pageNo, pageSize);
        Gson gson = new Gson();
        String s = gson.toJson(result);
        response.getWriter().print(s);

    }
}
