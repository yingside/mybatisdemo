package com.yingside.provider;

import com.yingside.po.Employee;
import org.apache.ibatis.jdbc.SQL;

import java.util.Map;

public class EmployeeProvider {

    public String selectBySelective(Map<String,Object> map){
        return new SQL(){
            {
                SELECT("*"); //select 列名
                FROM("t_emp"); //from t_emp
                RIGHT_OUTER_JOIN("t_dept on t_emp.fk_dept_id=t_dept.dept_id");//right outer join t_dept
                if(map.get("empName") != null){
                    AND().WHERE("emp_name like '%${empName}%'");
                }
                if(map.get("deptName") != null){
                    AND().WHERE("dept_name like '%${deptName}%'");
                }
            }
        }.toString();
    }
}
