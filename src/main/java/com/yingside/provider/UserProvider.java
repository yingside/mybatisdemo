package com.yingside.provider;

import com.yingside.po.User;
import org.apache.ibatis.jdbc.SQL;

public class UserProvider {
    //在这个方法中通过面向对象的方式写SQL语句，最后返回的都是SQL语句，也就是一个String
    public String updateUser(User user){
        return new SQL(){
            {
                UPDATE("t_user"); // "update t_user"
                if(user.getUserTel() != null){
                    SET("user_tel=#{userTel}"); // set user_tel=#{userTel}
                }
                if(user.getUsername() != null){
                    SET("username=#{username}"); // set username=#{username}
                }
                if(user.getPassword() != null){
                    SET("password=md5(#{password})"); // set password=md5(#{password})
                }
                if(user.getRegistrationTime() != null){
                    SET("registration_time=#{registrationTime}"); // set registration_time=#{registrationTime}
                }
                WHERE("pkid=#{id}");
            }
        }.toString();
    }

    public String addUser(User user){
        return new SQL(){
            {
                INSERT_INTO("t_user"); // insert into t_user
                VALUES("user_tel,username,password,registration_time","#{userTel},#{username},md5(#{password}),#{registrationTime}");
            }
        }.toString();
    }
}
