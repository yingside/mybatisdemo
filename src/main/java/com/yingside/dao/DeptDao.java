package com.yingside.dao;

import com.yingside.po.Dept;

import java.util.List;

public interface DeptDao {
    List<Dept> selectDeptEmployees();
}
