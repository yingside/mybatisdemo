package com.yingside.dao;

import com.yingside.po.Employee;
import com.yingside.po.User;
import com.yingside.provider.UserProvider;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface UserMapper {
    @Select("select * from t_user")
    @Results(id = "userMap", value={
            @Result(column = "pkid",property = "id"),
            @Result(column = "user_tel",property = "userTel"),
            @Result(column = "username",property = "username"),
            @Result(column = "password",property = "password"),
            @Result(column = "registration_time",property = "registrationTime")
    })
    List<User> getAll();

    @Select("select * from t_user where user_tel like '%${userTel}%' or username like '%${username}%'")
    @ResultMap(value="userMap")
    List<User> findUser(@Param("userTel") String phone, @Param("username") String name);


    @Delete("delete from t_user where pkid=#{id}")
    int delete(Integer id);

    @Insert("insert into t_user values(null,#{userTel},#{username},#{password},#{registrationTime})")
    @SelectKey(before = false,keyColumn = "pkid",keyProperty = "id",
            statement = "select last_insert_id()",resultType = Integer.class)
    int add(User user);

    @Update("update t_user" +
            "    set username = #{username}," +
            "      user_tel = #{userTel}," +
            "      password = md5(#{password})," +
            "      registration_time = #{registrationTime}" +
            "     where pkid=#{id}")
    int update(User user);

    @Update("<script>" +
            "update t_user" +
            "<set>" +
            "<if test=\"username!= null\"> " +
            "    username = #{username}," +
            "</if>" +
            "<if test=\"userTel!= null\"> " +
            "      user_tel = #{userTel}," +
            "</if>" +
            "<if test=\"password!= null\"> " +
            "      password = md5(#{password})," +
            "</if>" +
            "<if test=\"registrationTime!= null\"> " +
            "      registration_time = #{registrationTime}" +
            "</if>" +
            "</set>" +
            "     where pkid=#{id}" +
            "</script>")
    int updateSelective(User user);

    @UpdateProvider(type= UserProvider.class,method = "updateUser")
    int updateSelectiveProvider(User user);

    @InsertProvider(type= UserProvider.class,method = "addUser")
    int addProvider(User user);
}
