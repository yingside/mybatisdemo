package com.yingside.dao;

import com.yingside.po.User;

import java.util.List;

public interface UserDao {
    List<User> getAll();
    Integer addUser(User user);
}
