package com.yingside.dao;

import com.yingside.po.Employee;

import java.util.List;

public interface EmployeeDao {
    List<Employee> selectEmployees();
}
