package com.yingside.dao;

import com.yingside.po.Employee;
import com.yingside.provider.EmployeeProvider;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

import java.util.List;
import java.util.Map;

public interface EmployeeMapper {

    @Select("select * from t_emp")
    @Results(id = "empMap",value = {
            @Result(column = "emp_id",property = "empId",id = true),
            @Result(column = "emp_name",property = "empName"),
            @Result(column = "emp_tel",property = "empTel"),
            @Result(column = "emp_education",property = "empEducation"),
            @Result(column = "emp_birthday",property = "empBirthday"),
            @Result(column = "fk_dept_id",property = "dept"
                    ,one = @One(select = "com.yingside.dao.DeptMapper.getById",
                    fetchType = FetchType.LAZY))
    })
    List<Employee> getAll();

    @Select("select * from t_emp where fk_dept_id=#{deptId}")
    @ResultMap(value = "empMap")
    List<Employee> getEmployeesByDeptId(Integer deptId);

    @Select("select * from t_emp emp inner join t_dept dept on emp.fk_dept_id=dept.dept_id")
    @ResultMap(value = "com.yingside.mapper.EmployeeMapper2.empMap") //xml中配置的ResultMap
    List<Employee> getEmployeesByXML();

    @Select("select * from t_emp emp inner join t_dept dept on emp.fk_dept_id=dept.dept_id where emp.emp_name like '%${username}%'")
    @ResultMap(value = "com.yingside.mapper.EmployeeMapper2.empMap") //xml中配置的ResultMap
    List<Employee> getEmployeesByParam(@Param("username") String username);


    @SelectProvider(type = EmployeeProvider.class,method = "selectBySelective")
    @ResultMap(value = "com.yingside.mapper.EmployeeMapper2.empMap") //xml中配置的ResultMap
    List<Employee> getEmployeesByParamProvider(Map<String,Object> map);

}
