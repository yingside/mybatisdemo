package com.yingside.dao;

import com.yingside.po.Dept;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.mapping.FetchType;

import java.util.List;

public interface DeptMapper {
    @Select("select * from t_dept where dept_id=#{id}")
    @Results(id="deptMap",value = {
            @Result(column = "dept_id",property = "deptId",id = true),
            @Result(column = "dept_name",property = "deptName"),
            @Result(column = "dept_info",property = "deptInfo"),
            @Result(column = "dept_createDate",property = "deptCreateDate")
    })
    public Dept getById(Integer id);


    @Select("select * from t_dept")
    @Results(id="deptMap2",value = {
            @Result(column = "dept_id",property = "deptId",id = true),
            @Result(column = "dept_name",property = "deptName"),
            @Result(column = "dept_info",property = "deptInfo"),
            @Result(column = "dept_createDate",property = "deptCreateDate"),
            @Result(column = "dept_id", property = "employeeList",
                many = @Many(
                        select = "com.yingside.dao.EmployeeMapper.getEmployeesByDeptId",
                        fetchType= FetchType.LAZY))
    })
    public List<Dept> getAllDept();
}
