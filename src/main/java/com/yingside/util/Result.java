package com.yingside.util;

import lombok.Data;

@Data
public class Result<T> {
    private Integer code;
    private Long count;
    private String message;
    private T data;
}
