package com.yingside.util;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

public class SqlSessionFactoryUtil {
    private static SqlSessionFactory sqlSessionFactory;
    private SqlSessionFactoryUtil(){}

    public synchronized static SqlSessionFactory getSqlSessionFactory(){
        if(sqlSessionFactory == null){
            String resource = "mybatis-config.xml";
            try {
                InputStream ios = Resources.getResourceAsStream(resource);
                sqlSessionFactory = new SqlSessionFactoryBuilder().build(ios);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sqlSessionFactory;
    }
}
