package com.yingside.util;

import org.apache.ibatis.session.SqlSession;

public class SqlSessionUtil {
    //ThreadLocal 就相当于一个java封装好的线程池
    private static ThreadLocal<SqlSession> threadLocal = new ThreadLocal<>();

    public static SqlSession getSqlSession(){
        SqlSession sqlSession = threadLocal.get();
        if(sqlSession == null){
            sqlSession = SqlSessionFactoryUtil.getSqlSessionFactory().openSession();
            threadLocal.set(sqlSession);
        }
        return sqlSession;
    }

    public static void commit(){
        SqlSession sqlSession = threadLocal.get();
        if(sqlSession != null){
            sqlSession.commit();
            threadLocal.set(null);
        }
    }

}
