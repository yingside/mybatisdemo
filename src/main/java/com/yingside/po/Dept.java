package com.yingside.po;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class Dept implements Serializable {
    private int deptId;
    private String deptName;
    private String deptInfo;
    private Date deptCreateDate;
    private List<Employee> employeeList;
}