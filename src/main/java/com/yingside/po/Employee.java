package com.yingside.po;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class Employee implements Serializable {
    private int empId;
    private String empName;
    private String empTel;
    private String empEducation;
    private Date empBirthday;
    private Dept dept;
}
