package com.yingside.po;

import java.io.Serializable;

public class User implements Serializable {
    private Integer id;
    private String userTel;
    private String username;
    private String password;
    private String registrationTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserTel() {
        return userTel;
    }

    public void setUserTel(String userTel) {
        this.userTel = userTel;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRegistrationTime() {
        return registrationTime;
    }

    public void setRegistrationTime(String registrationTime) {
        this.registrationTime = registrationTime;
    }

    public User() {
    }

    public User(Integer id, String userTel, String username, String password, String registrationTime) {
        this.id = id;
        this.userTel = userTel;
        this.username = username;
        this.password = password;
        this.registrationTime = registrationTime;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", userTel='" + userTel + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", registrationTime='" + registrationTime + '\'' +
                '}';
    }
}
