package com.yingside.po;

import java.io.Serializable;
import java.util.List;

public class QueryBean implements Serializable {
    private List<Integer> ids;
    private String beginTime;
    private String endTime;

    public List<Integer> getIds() {
        return ids;
    }

    public void setIds(List<Integer> ids) {
        this.ids = ids;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
